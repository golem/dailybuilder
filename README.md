# dailybuilder for GOLEM
Daily builds given repositories.

## Usage
Just copy (or soft link) an example config file in repos-enabled directory, and edit as you like.
Use comments to understand meaning of various options.

## How it works
cron must call the dailybuilder.sh bash script at least once per day

dailybuilder.sh retrieves its settings in config.sh script.

Then it searchs for repositories config files in repos-enabled directory, and runs lib/build.sh for each of them.

build.sh exctracts the current repository settings from its config file, then looks for the $REPONAME file inside $LAST_PULL_DIR: if does not exist, then this is the first time that build is required for this repository, so it clones it and also prepares an output directory in the $OUTPUT_DIR directory. Else, if file exists, it means that the repository has already been pulled in the past, and file contains the Unix timestamp representing this event. So build.sh simply does a math calc to determine wether its "build time" is expired (eg, build.sh is run after 60 minutes from last pull of a daily repository, so nothing has to be done; eg, build.sh is run after 25hours from last pull of a daily repository, so the repository is flagged to be built).

If repository is flagged to be built, then build.sh changes its current path directory and goes inside the repository root, then pulls updates (if any), executes the given command in the repository config file (eg. ./configure && make), then copies the ojects given in the config file to the output dir (eg. build/release/binary.elf) and finally updates last Unix timestamp.

Tip: to test config, dailybuilder.sh can also be run manually in its directory.
Note that this could trigger the build of configured repositories and take some time, at least on first manual run.

Warning: no safety checks are done, so be very careful when doing anything!

Note: of course, the system that runs the dailybuilder must have installed
all the dependencies needed for the configured repositories.

Crontab example:
30 * * * * cd ~/bin/dailybuilder/ && ./dailybuilder.sh

