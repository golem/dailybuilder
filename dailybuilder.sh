#!/bin/bash

. config.sh

find "$REPOS_ENABLED_DIR" -iname "*.conf" -exec lib/build.sh {} \;
