#!/bin/bash

. config.sh

STREAM=$(grep -v -e "^#" -e "^\s*$" "$1")

REPONAME=$(echo "$STREAM" | grep 'name:' | cut -d':' -f2- | sed -e 's/^[ \t]*//')
ADDRESS=$(echo "$STREAM" | grep 'address:' | cut -d':' -f2- | sed -e 's/^[ \t]*//')
COMMAND=$(echo "$STREAM" | grep 'command:' | cut -d':' -f2- | sed -e 's/^[ \t]*//')
OBJECTS=$(echo "$STREAM" | grep 'objects:' | cut -d':' -f2- | sed -e 's/^[ \t]*//')
RATE=$(echo "$STREAM" | grep 'rate:' | cut -d':' -f2- | sed -e 's/^[ \t]*//')

case "$RATE" in
	d)	# daily
	RATESEC="86400"
	;;
	w)	# weekly
	RATESEC="604800"
	;;
	m)	# monthly
	RATESEC="2592000"
	;;
	*)	# default, every time this script is run
	RATESEC="0"
	;;
esac

echo "Repository name: $REPONAME"
echo "Last pull dir: $LAST_PULL_DIR"

# If it has been pulled before, decide wether to pull or not
if [ -f "$LAST_PULL_DIR/$REPONAME" ]; then
	LASTPULL=$(cat "$LAST_PULL_DIR/$REPONAME")
	if [ $(("$LASTPULL" + "$RATESEC")) -le $(date +"%s") ]; then
		echo "dailybuilder: repository needs to be built"
		TO_BUILD="yes"
	else
		TO_BUILD="no"
		echo "dailybuilder: nothing to do"
	fi
# If it was never pulled before, clone
else
	echo "dailybuilder: first clone..."
	git clone "$ADDRESS" "$REPOS_DIR/$REPONAME/"
	mkdir -p "$OUTPUT_DIR/$REPONAME"
	TO_BUILD="yes"
fi

# Actually build
if [ "$TO_BUILD" == "yes" ]; then
	echo "dailybuilder: cloning... $REPONAME"
	cd "$REPOS_DIR/$REPONAME"
	git pull
	eval "$COMMAND"
	cp -r "$OBJECTS" "$OUTPUT_DIR/$REPONAME"
	git log | sed 's/<.*>//g' > "$OUTPUT_DIR/$REPONAME/log.txt"
	date +"%s" > "$LAST_PULL_DIR/$REPONAME"
	touch "$OUTPUT_DIR/$REPONAME"
fi
