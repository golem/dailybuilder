# last-pull
This directory will be populated with information about last pull time of each repository.
For each repository, a file will be created with the same name of the repository and its content will be the Unix timestamp of last pull.

